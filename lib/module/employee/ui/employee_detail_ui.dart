import 'package:flutter/material.dart';
import 'package:white_rabit/common/color.dart';
import 'package:white_rabit/module/employee/model/employee_model.dart';
import 'package:white_rabit/widgets/my_text_widget.dart';
import 'package:white_rabit/widgets/space_widget.dart';

class EmployeeDetails extends StatefulWidget {
  final EmployeeModel employeeModel;

  const EmployeeDetails({Key? key, required this.employeeModel})
      : super(key: key);

  @override
  _EmployeeDetailsState createState() => _EmployeeDetailsState();
}

class _EmployeeDetailsState extends State<EmployeeDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
      appBar: AppBar(
        title: MyTextWidget(widget.employeeModel.username ?? ""),
      ),
    );
  }

  Widget _body() {
    return Column(
      children: [
        spaceWidget(),
        Center(
          child: CircleAvatar(
            backgroundImage: NetworkImage(
              widget?.employeeModel?.profileImage ?? "",
            ),
            radius: 60,
          ),
        ),
        spaceWidget(),
        MyTextWidget(widget.employeeModel.name ?? "", size: 20),
        spaceWidget(),
        MyTextWidget(widget.employeeModel.email ?? "", size: 14),
        spaceWidget(),
        MyTextWidget(
            "${widget.employeeModel.address?.city ?? ""},"
            " ${widget.employeeModel.address?.street ?? ""}"
            "\n ${widget.employeeModel.address?.suite ?? ""}"
            "\n ${widget.employeeModel.address?.zipcode ?? ""}",
            size: 16),
        spaceWidget(),
        widget.employeeModel.phone == null
            ? Container()
            : SizedBox(
                width: MediaQuery.of(context).size.width / 1.2,
                child: ListTile(
                  leading: const Icon(
                    Icons.phone,
                    color: EMP_WHITE,
                  ),
                  title:
                      MyTextWidget(widget.employeeModel.phone ?? "", size: 20),
                ),
              ),
        spaceWidget(),
        SizedBox(
          width: MediaQuery.of(context).size.width / 1.2,
          child: ListTile(
            leading: const Icon(
              Icons.work,
              color: EMP_WHITE,
            ),
            title: MyTextWidget(widget.employeeModel.company?.bs ?? "",
                size: 20),
          ),
        ),
      ],
    );
  }
}
