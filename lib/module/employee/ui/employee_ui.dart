import 'package:flutter/material.dart';
import 'package:white_rabit/common/color.dart';
import 'package:provider/provider.dart';
import 'package:white_rabit/widgets/loader_widget.dart';
import 'package:white_rabit/widgets/my_text_widget.dart';

import '../employee_provider.dart';
import 'employee_detail_ui.dart';

class EmpUI extends StatefulWidget {
  const EmpUI({Key? key}) : super(key: key);

  @override
  _EmpUIState createState() => _EmpUIState();
}

class _EmpUIState extends State<EmpUI> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Emplyees",
          style: TextStyle(color: EMP_WHITE),
        ),
        // backgroundColor: EMP_ERROR_LIGHT,
        elevation: 0.0,
      ),
      body: _empList(),
    );
  }

  Widget _empList() =>
      Consumer<CategoryProvider>(builder: (context, provider, child) {
        return provider.loading
            ? loaderWidget()
            : provider.employeeModelList.isNotEmpty
                ? ListView.builder(
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => EmployeeDetails(
                                      employeeModel:
                                          provider.employeeModelList[index],
                                    )),
                          );
                        },
                        child: ListTile(
                          leading: CircleAvatar(
                            backgroundImage: NetworkImage(provider
                                    .employeeModelList[index].profileImage ??
                                ""),
                          ),
                          title: MyTextWidget(
                            provider.employeeModelList[index].name ?? "",
                          ),
                          subtitle: MyTextWidget(
                              provider.employeeModelList[index].company?.name ??
                                  ""),
                        ),
                      );
                    },
                    itemCount: provider.employeeModelList.length,
                  )
                : const Center(
                    child: Text("No List available"),
                  );
      });

  @override
  void initState() {
    Future.microtask(
      () => context.read<CategoryProvider>().renderEmployees(),
    );
    super.initState();
  }
}
