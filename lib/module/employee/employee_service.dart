import 'package:dio/dio.dart';
import 'package:white_rabit/common/service.dart';

class CategoryService {
  Future<Response> getCategories() async {
    Response _data = await ServiceUtils().get('5d565297300000680030a986');
    // List<CategoryDTO> _categories = parseCategories(_data['category']);
    // List<AppInfoDTO> _apps = parseApps(_data['apps']);
    // return {"categories": _categories, "apps": _apps};
    return _data;
  }
}
