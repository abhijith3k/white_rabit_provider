import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:white_rabit/common/service_locator/employee_sl.dart'
    as slemployee;
import 'package:white_rabit/module/employee/ui/employee_ui.dart';

import 'employee_provider.dart';

class Employee extends StatefulWidget {
  @override
  EmployeeState createState() => EmployeeState();
}

class EmployeeState extends State<Employee> {
  @override
  void initState() {
    slemployee.setup();
    super.initState();
  }

  @override
  void dispose() {
    slemployee.reset();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CategoryProvider>(
      create: (context) => CategoryProvider(),
      builder: (context, _) => EmpUI(),
    );
  }
}
