import 'package:flutter/foundation.dart';
import 'package:white_rabit/common/toast.dart';
import 'package:white_rabit/common/service_locator/employee_sl.dart'
    as slemployee;

import 'employee_service.dart';
import 'model/employee_model.dart';
// import 'package:eportal/common/service_locator/category_sl.dart' as slCategory;

class CategoryProvider with ChangeNotifier {
  bool loading = false;
  List<EmployeeModel> employeeModelList = [];

  Future renderEmployees() async {
    try {
      loading = true;
      notifyListeners();
      var _data = await slemployee.instance<CategoryService>().getCategories();
      print(_data.data);

      for (var _emp in _data.data) {
        employeeModelList.add(EmployeeModel.fromJson(_emp!));
      }
      debugPrint("employeeModelList length - ${employeeModelList.length}");

      // categories = _data["categories"];
      // apps = _data["apps"];
      loading = false;
      notifyListeners();
    } catch (err) {
      loading = false;
      notifyListeners();
      ToastAlert().show("Failed to get Categores");
    }
  }
}
