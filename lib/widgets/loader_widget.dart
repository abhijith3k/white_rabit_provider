import 'package:flutter/material.dart';
import 'package:white_rabit/common/color.dart';

Widget loaderWidget() {
  return Center(
    child: CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(EMP_BLACK)),
  );
}
