import 'package:flutter/material.dart';
import 'package:white_rabit/common/color.dart';

Widget MyTextWidget(String? text, {double? size}) => Text(
      text ?? "",
      style: TextStyle(
        color: EMP_WHITE,
        fontSize: size,
      ),
      textAlign: TextAlign.center,
    );
