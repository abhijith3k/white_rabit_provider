import 'package:flutter/material.dart';

Widget spaceWidget({double? size}) {
  return SizedBox(
    height: size ?? 20,
  );
}
