import 'package:flutter/material.dart';
import 'package:white_rabit/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(Eportal());
}

class Eportal extends StatefulWidget {
  @override
  _EportalState createState() => _EportalState();
}

class _EportalState extends State<Eportal> {
  @override
  Widget build(BuildContext context) {
    return Routes();
  }
}
