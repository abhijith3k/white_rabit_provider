import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:white_rabit/common/color.dart';

enum ToastType { success, error, warning }

class ToastAlert {
  void show(message, {type = ToastType.error}) {
    Fluttertoast.showToast(
        fontSize: 14,
        textColor: _getTextColor(type),
        backgroundColor: _getBackgroundColor(type),
        msg: message,
        gravity: ToastGravity.BOTTOM);
  }

  Color _getTextColor(ToastType _type) {
    switch (_type) {
      case ToastType.error:
        return EMP_ERROR;
      case ToastType.warning:
        return Colors.orangeAccent;
      case ToastType.success:
      default:
        return EMP_PRIMARY;
    }
  }

  Color _getBackgroundColor(ToastType _type) {
    switch (_type) {
      case ToastType.error:
        return EMP_ERROR_LIGHT;
      case ToastType.warning:
        return Colors.orangeAccent;
      case ToastType.success:
      default:
        return EMP_PRIMARY_LIGHT;
    }
  }
}
