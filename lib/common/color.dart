import 'package:flutter/material.dart';

const EMP_WHITE = Color.fromRGBO(255, 255, 255, 1);
const EMP_BLACK = Colors.black;
const EMP_BLACK_MEDIUM = Colors.black54;
const EMP_PRIMARY = Color.fromRGBO(0, 148, 222, 1);
const EMP_PRIMARY_LIGHT = Color.fromRGBO(179, 230, 255, 1);
const EMP_PRIMARY_SHADE = Color.fromRGBO(0, 148, 222, 0.5);
const EMP_PRIMARY_DARK = Color.fromRGBO(0, 85, 128, 1);
const EMP_ERROR = Color.fromRGBO(255, 0, 0, 1);
const EMP_ERROR_LIGHT = Color.fromRGBO(255, 204, 204, 1);
const EMP_GREY = Color.fromRGBO(128, 128, 128, 1);
const EMP_DARK_GREY = Color.fromRGBO(66, 66, 66, 1);
const EMP_SOFT_GREY = Color.fromRGBO(235, 235, 235, 1);
const EMP_MEDIUM_GREY = Color.fromRGBO(128, 128, 128, 1);
const EMP_YELLOW = Color.fromRGBO(247, 188, 0, 1);