import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:connectivity/connectivity.dart';

import 'Env.dart';

class ServiceUtils {
  checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  static ServiceUtils _instance = new ServiceUtils.internal();

  ServiceUtils.internal();

  factory ServiceUtils() => _instance;

  Future<dynamic> get(endPoint, {queryParameters}) async {
    return caller(endPoint, "GET", queryParameters, null, null);
  }

  Future<dynamic> post(endPoint, body) {
    return caller(
      endPoint,
      "POST",
      null,
      body,
      null,
    );
  }

  Future<dynamic> put(endPoint, body, {queryParameters}) {
    return caller(endPoint, "PUT", queryParameters, body, null);
  }

  caller(
    endPoint,
    method,
    queryParameters,
    data,
    headers,
  ) async {
    Dio dio = Dio();
    // debugPrint("TOKEN - ${_token}");
    Options _options = Options();
    _options.method = method;
    _options.headers = headers != null
        ? headers
        : {
            "Content-Type": "application/json",
          };
    try {
      Response response = await dio.request(
        API_URL + endPoint,
        data: data,
        options: _options,
        queryParameters: queryParameters,
      );

      debugPrint("request Res - ${response.data}");
      return response;
    } on DioError catch (err) {
      print(err);
      var decoded = json.decode(err.response.toString());
      throw (Map<String, dynamic>.from(decoded));
    } catch (err) {
      print(err);
      var decoded = json.decode(err.toString());
      throw (Map<String, dynamic>.from(decoded));
    }
  }
}
