import 'package:get_it/get_it.dart';
import 'package:white_rabit/module/employee/employee_service.dart';

GetIt instance = GetIt.instance;

void setup() {
  instance.allowReassignment = true;
  instance.registerFactory<CategoryService>(() => CategoryService());
}

void reset() {
  // instance.reset();
}
